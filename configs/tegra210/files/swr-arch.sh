#!/bin/bash

# Pre install configurations
## Workaround for flakiness of `pt` mirror.
sed -i 's/http:\/\/mirror.archlinuxarm.org/http:\/\/eu.mirror.archlinuxarm.org/g' /etc/pacman.d/mirrorlist

# Initialize pacman keyring
pacman-key --init
pacman-key --populate archlinuxarm

## Arch switchroot repository
echo -e "[linux-4-switch]\nServer = https://repo.azka.li/arch/" >> /etc/pacman.conf
curl https://repo.azka.li/pubkey > /tmp/pubkey
pacman-key --add /tmp/pubkey
pacman-key --lsign-key D91DE9A62EE8AF1DD943ECE1F7B322ACCF81D19A

# Installation
pacman -Rdd linux-aarch64 --noconfirm

echo -e "\n\nBeginning packages installation!"
pacman -Syyu --noconfirm --overwrite="*" nintendo-switch-meta

# Post install configurations
yes | pacman -Scc

usermod -aG video,audio,wheel alarm
